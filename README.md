# LeasePlanAssignment Serenity and Cucumber

### The project directory structure
The project has build scripts for both Maven and Gradle, and follows the standard directory structure used in most Serenity projects:
```Gherkin
src
  + main
  + test
    + java                        Test runners and supporting code
    + resources
      + features                  Feature files
    + search                  Feature file subdirectories
             search_by_keyword.feature
```

LeasePlan BE QA AUTO TEST ASSIGNMENT.


**Installation**

1. Clone the project via the command below.

   `git clone https://gitlab.com/LeasePlanAssignmentV2/auwiar.git`

   In case you get an error "`command not found: git`", please install git to your machine. 
   
   Kindly check resources below.

   [Download GIT](https://git-scm.com/downloads)

   [Digital Ocean blog about GIT](https://www.digitalocean.com/community/tutorials/how-to-contribute-to-open-source-getting-started-with-git)
    
   [Atlassian GIT tutorial](https://www.atlassian.com/git/tutorials/install-git) 


2. Once you have cloned the repository, open it via your IDE(Maven supporting). I am using eclipse.

3. Enter the command below to install dependencies and start testing your project.(It is running the test file because of the maven hierarchy.)

   `mvn clean install`
*The test results will be recorded here `target/site/serenity/index.html`.*

4. Lastly, you should configure your Maven runner to run test project.

   - Go to "Run" on your toolbar.
   - Click "Edit Configurations".
   - Click "+" icon on top left.
   - Click "Maven" on left hand side of window.
   - Assign a name to your configuration or leave it as default.(In my case it is artifactId in pom.xml file)
   - Paste the line below to [Command Line] input field.

      `prettier:write clean verify serenity:aggregate`
      
   - Click "Apply" and click "OK" buttons.


**Run Current Tests**

1. Simply click "Run" on toolbar and click "Run" button on the list.(Check that run is executed via added configuration.)


**Contribute Your Scenarios to Feature File**

1. Select your desired steps from the path below.
    > leaseplanassignment/src/test/java/starter/stepdefinitions/SearchStepDefinitions.java

2. Go to file below.
   > leaseplanassignment/src/test/resources/features/search/search_product.feature

3. Name your scenario with the definers below.
 
   - In case you have a data table to provide for your scenario, name it as > Scenario Outline
   - In case you **don't** need to provide a data table, name it as > Scenario

     `Scenario Outline: As a API user, when I call valid product endpoints, I should get corresponding values`


4. Add your test steps with the corresponding step definer(Given, When, Then).  

   Plus, replace {} placeholders with the parameter names which you define in data table. 

   `When I searche for <product>`
   
   `Then Verify that the response code is <status>`
    Note : The given products' apple,mango,tofu,water is not available, so the api response will not be as expected. But still covered this scenario in positive case but when execute this scenario will be failied

5. In case you need to provide a data table, write it below to your scenario as given the example below.(Including "Examples" line)

   Examples:
      | product |
      | apple   | 
      | orange  | 
      | pasta   |
      | cola    |

6. If you would like to add more step definitions, please check the documentation of Serenity BDD.

   [Serenity BDD Documentation](https://serenity-bdd.info/documentation/)

# Changes

1. I have changed the naming of **post_product.feature** file to **search_product.feature**

2. I have changed the naming of **CarsAPI** to **SearchAPI** as per the objective of API.

3. I have removed out redundant **gradle** files in project directory.

4. I have added **prettier** dependency in **pom.xml** and its configuration to prettify the files.

5. I have added **Hooks** file to initialise baseUrl before starting to run scenarios.

6. I have added gitlab-ci configuration file to run the tests in pipeline and have the results as artifacts.

7. I have added tags to create aggregation and to differentiate scenarios from each other.

8. I have populated README.md file with some useful information.