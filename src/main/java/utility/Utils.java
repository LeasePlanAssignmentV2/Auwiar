package utility;

import java.io.FileReader;
import java.util.Properties;

public class Utils {
	// public static Logger logger = Logger.getLogger(Utils.class);

	public static String getProperties(String property) {

		try {
			FileReader reader = new FileReader("src/test/resources/serenity.conf");
			Properties properties = new Properties();
			properties.load(reader);
			System.out.println(properties.getProperty(property));
			property = properties.getProperty(property);
			// logger.info("config file called for properties");
		}

		catch (Exception e) {
			System.out.println(e);
		}

		return property;

	}

}
