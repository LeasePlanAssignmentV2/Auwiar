package hooks;

import java.io.IOException;

import io.cucumber.java.Before;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import utility.Utils;

public class Hooks extends Utils{
	
	private EnvironmentVariables env;
	@Before
	    public String setup() throws IOException {
	        
		    env = SystemEnvironmentVariables.createEnvironmentVariables();
	        String baseUri= EnvironmentSpecificConfiguration.from(env).getProperty("baseUrl");
	        return baseUri;
	}

}
