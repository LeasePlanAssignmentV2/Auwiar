package stepDefinitions;

import static io.restassured.RestAssured.given;

import org.junit.Assert;

import hooks.Hooks;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class StepDefinitions {
	
	Hooks hook = new Hooks();

	RequestSpecification request;
	Response response;

	@When("I searche for orange")
	public void i_searche_for_orange() throws Exception {
		response = given().when().get(hook.setup() + "/api/v1/search/demo/orange");
		System.out.println("Response status code for the orange product is: " + response.getStatusCode());
	}

	@Then("Verify that the response code is {int}")
	public void verify_that_the_response_code_is(int status) throws Exception {
		response = given().when().get(hook.setup() + "/api/v1/search/demo/orange");
		Assert.assertEquals(status, 200);
	}

	@Then("Verify that the product orange is present in search results")
	public void verify_that_the_product_orange_is_present_in_search_results() {
		// Write code here that turns the phrase above into concrete actions
	}

	@When("I searche for apple")
	public void i_searche_for_apple() throws Exception {
		response = given().when().get(hook.setup() + "/search/demo/apple");
		System.out.println("Response status code for the apple product is: " + response.getStatusCode());
	}

	@Then("Verify that the product apple is present in search results")
	public void verify_that_the_product_apple_is_present_in_search_results() {
		// Write code here that turns the phrase above into concrete actions
	}

	@When("I searche for pasta")
	public void i_searche_for_pasta() throws Exception{
		response = given().when().get(hook.setup() + "/search/demo/pasta");
		System.out.println("Response status code for the pasta product is: " + response.getStatusCode());
	}

	@Then("Verify that the product pasta is present in search results")
	public void verify_that_the_product_pasta_is_present_in_search_results() {
		// Write code here that turns the phrase above into concrete actions
	}

	@When("I searche for cola")
	public void i_searche_for_cola() throws Exception {
		response = given().when().get(hook.setup() + "/search/demo/apple");
		System.out.println("Response status code for the cola product is: " + response.getStatusCode());
	}

	@Then("Verify that the product cola is present in search results")
	public void verify_that_the_product_cola_is_present_in_search_results() {
		// Write code here that turns the phrase above into concrete actions
	}

	@When("I search for car")
	public void i_search_for_car() throws Exception {

		response = given().when().get(hook.setup() + "/search/demo/car");
		System.out.println("Response status code for the car product is: " + response.getStatusCode());
		
		//restAssuredThat(response -> response.body("error", contains("True")));

	}

	@Then("Verify that the response {int} is not found")
	public void verify_that_the_response_is_not_found(int status) {
		Assert.assertEquals(status, 404);
		System.out.println("Car status code is: " + status);

	}

}
