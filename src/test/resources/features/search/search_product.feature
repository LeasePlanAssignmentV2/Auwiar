### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

@feature:search
Feature: Search for the product

@search @positive
  Scenario Outline: Test senario get response status and available product
    When I searche for <product>
    Then Verify that the response code is <status>
    And Verify that the product <product> is present in search results
    Examples:
      | product | status | 
      | orange  | 200    |
      | apple   | 200    | 
      | pasta   | 200    |  
      | cola    | 200    |  
  
  @search @negative
  Scenario Outline: Test senario get negative response status and not available product
    When I search for <product>
    Then  Verify that the response <status> is not found
    Examples:
      | product | status|
      | car     | 404        |   				     
